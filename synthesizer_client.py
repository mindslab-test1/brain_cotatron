import grpc
import argparse
import google.protobuf.empty_pb2 as empty

from cotatron_pb2 import WavTxtTargetspk
from cotatron_pb2_grpc import MelConversionStub


class MelConversionClient(object):
    def __init__(self, remote='127.0.0.1:30002', chunk_size=3145728):
        channel = grpc.insecure_channel(remote)
        self.stub = MelConversionStub(channel)
        self.chunk_size = chunk_size

    def wavtxt2mel(self, wav_binary, text, speaker):
        message = self._generate_wav_binary_iterator(wav_binary, text, speaker)
        return self.stub.WavTxtTargetspk2Mel(message)

    def _generate_wav_binary_iterator(self, wav_binary, text, speaker):
        for idx in range(0, len(wav_binary), self.chunk_size):
            yield WavTxtTargetspk(
                wav_source=wav_binary[idx:idx+self.chunk_size], text=text, spk_target=speaker)

    def get_mel_config(self):
        return self.stub.GetMelConfig(empty.Empty())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='cotatron-based mel conversion client')
    parser.add_argument('-r', '--remote', type=str, default='127.0.0.1:30002',
                        help="grpc: ip:port")
    parser.add_argument('-i', '--input', type=str, required=True,
                        help="input wav file")
    parser.add_argument('-t', '--text', type=str, required=True,
                        help="input text")
    parser.add_argument('-s', '--speaker', type=int, required=True,
                        help="target speaker index")
    args = parser.parse_args()

    client = MelConversionClient(args.remote)

    with open(args.input, 'rb') as rf:
        wav_binary = rf.read()

    result = client.wavtxt2mel(wav_binary, args.text, args.speaker)
    for data in result:
        print(data)

    print(client.get_mel_config())

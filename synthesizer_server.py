import logging
import time
import torch
import torch.nn as nn
import grpc
import argparse
import librosa
import numpy as np
from omegaconf import OmegaConf
from concurrent import futures
from scipy.io.wavfile import read

from datasets.text import Language
from synthesizer import Synthesizer
from utils.utils import WavBinaryWrapper

from cotatron_pb2 import WavTxtTargetspk, MelSpectrogram, MelConfig
from cotatron_pb2_grpc import add_MelConversionServicer_to_server, MelConversionServicer

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class CotatronDecoderImpl(MelConversionServicer):
    def __init__(self, checkpoint_path, device, mel_chunk_size, config_paths, ref_wavlist):
        super().__init__()
        torch.cuda.set_device(device)
        self.device = device
        self.mel_chunk_size = mel_chunk_size

        args = self._gen_hparams(config_paths)
        self.model = Synthesizer(args).to(device)
        self.hp = self.model.hp
        self.lang = Language(self.hp.data.lang, self.hp.data.text_cleaners)

        self._load_checkpoint(checkpoint_path)

        self.speaker_dict = None
        ref_wavlist = OmegaConf.load(ref_wavlist)['reference_wavlist']
        self._gen_speaker_dict(ref_wavlist)

    def _gen_hparams(self, config_paths):
        # generate hparams object for pl.LightningModule
        parser = argparse.ArgumentParser()
        parser.add_argument('--config')
        args = parser.parse_args(['--config', config_paths])
        return args

    def _load_checkpoint(self, checkpoint_path):
        checkpoint = torch.load(checkpoint_path, map_location='cpu')
        self.model.load_state_dict(checkpoint['state_dict'])
        self.model.eval()
        self.model.freeze()
        del checkpoint
        torch.cuda.empty_cache()

    @torch.no_grad()
    def _gen_speaker_dict(self, reference_wavlist):
        self.speaker_dict = nn.Embedding(len(reference_wavlist), self.hp.chn.speaker.token)
        self.speaker_dict = self.speaker_dict.to(self.device)
        for idx, wavpath in enumerate(reference_wavlist):
            wav, sr = librosa.load(wavpath, sr=None)
            assert sr == self.hp.audio.sampling_rate, \
                'sampling_rate mismatch at %s: expected %d, got %d' \
                % (wavpath, self.hp.audio.sampling_rate, sr)

            wav *= (0.99 / np.max(np.abs(wav)))
            wav = torch.from_numpy(wav).view(1, 1, -1).to(self.device)
            mel = self.model.cotatron.audio2mel(wav)
            z = self.model.speaker.inference(mel)
            self.speaker_dict.weight[idx] = z

    @torch.no_grad()
    def WavTxtTargetspk2Mel(self, wav_txt_targetspk, context):
        try:
            torch.cuda.set_device(self.device)
            mel_s_t = self._inference(wav_txt_targetspk, context)
            sample_length = mel_s_t.size(-1) * self.hp.audio.hop_length
            for mel_chunk in torch.split(mel_s_t, self.mel_chunk_size, dim=-1):
                mel_outputs = mel_chunk.reshape(-1).cpu().tolist()
                mel = MelSpectrogram(data=mel_outputs, sample_length=sample_length)
                yield mel

        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @torch.no_grad()
    def _inference(self, wav_txt_targetspk, context):
        is_first = True
        wav_source = bytearray()
        text = ""
        target_spk_index = -1
        for wav_txt_spk_iterator in wav_txt_targetspk:
            if is_first:
                text = wav_txt_spk_iterator.text
                target_spk_index = wav_txt_spk_iterator.spk_target
                is_first = False
            wav_source.extend(wav_txt_spk_iterator.wav_source)

        # Handle exceptions
        if len(wav_source) == 0:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("got empty wav_source")
            return None

        if target_spk_index not in range(self.speaker_dict.num_embeddings):
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("invalid speaker number: expected %d-%d, got %d"
                % (0, self.speaker_dict.num_embeddings-1, target_spk_index))
            return None

        if len(text) == 0:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("got empty text sequence")
            return None

        # convert input(wav, text, speaker) into torch tensors
        text = torch.LongTensor(self.lang.text_to_sequence(text, self.hp.data.text_cleaners))
        text = text.unsqueeze(0).to(self.device)

        wav = WavBinaryWrapper(wav_source)
        sr, wav = read(wav)
        if sr != self.hp.audio.sampling_rate:
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details("sampling rate mismatch: expected %d, got %d"
                % (sr, self.hp.audio.sampling_rate))
            return None

        if len(wav.shape) == 2:
            wav = wav[:, 0]

        if wav.dtype == np.int16:
            wav = wav / 32768.0
        elif wav.dtype == np.int32:
            wav = wav / 2147483648.0
        elif wav.dtype == np.uint8:
            wav = (wav - 128) / 128.0

        wav = wav.astype(np.float32)
        wav = torch.from_numpy(wav).to(self.device)
        wav = wav.view(1, 1, -1)
        wav *= (0.99 / torch.max(torch.abs(wav)))
        mel_source = self.model.cotatron.audio2mel(wav)

        target_spk_index = torch.tensor([target_spk_index], device=self.device)
        z_t = self.speaker_dict(target_spk_index)

        mel_s_t, _, _ = self.model.inference_from_z_t(text, mel_source, z_t)
        return mel_s_t

    def GetMelConfig(self, empty, context):
        mel_config = MelConfig(
            filter_length=self.hp.audio.filter_length,
            hop_length=self.hp.audio.hop_length,
            win_length=self.hp.audio.win_length,
            n_mel_channels=self.hp.audio.n_mel_channels,
            sampling_rate=self.hp.audio.sampling_rate,
            mel_fmin=self.hp.audio.mel_fmin,
            mel_fmax=self.hp.audio.mel_fmax,
        )
        return mel_config


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='cotatron-based mel conversion executor')
    parser.add_argument('-c', '--config', nargs=2, type=str, required=True,
                        help="path of configuration yaml file")
    parser.add_argument('-r', '--reference_wavlist', type=str, required=True,
                        help="path of yaml for list of reference wavpath")
    parser.add_argument('-m', '--model', type=str, required=True,
                        help="path of checkpoint(model)")
    parser.add_argument('-l', '--log_level', type=str, default='INFO',
                        help="logger level")
    parser.add_argument('-p', '--port', type=int, default=30002,
                        help="grpc port")
    parser.add_argument('-d', '--device', nargs='?', type=int, default=0,
                        help="gpu device")
    parser.add_argument('-w', '--max_workers', type=int, default=1,
                        help="max workers")
    parser.add_argument('--mel_chunk_size', type=int, default=88,
                        help="mel chunk size")
    args = parser.parse_args()

    synthesizer = CotatronDecoderImpl(args.model, args.device, args.mel_chunk_size, args.config, args.reference_wavlist)
    
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers), )
    add_MelConversionServicer_to_server(synthesizer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('cotatron-based mel conversion starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)

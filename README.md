# brain_cotatron

Cotatron-based Voice Conversion

## Build
```bash
docker build -f Dockerfile -t cotatron:<version> .
docker build -f Dockerfile-server --build-arg cotatron_version=<version> -t cotatron:<version>-server .
```

## Inference (Serving)

```bash
docker run --gpus device=0 -d -v <checkpoint_config_directory>:/model -p 30002:30002 \
-e COTATRON_GLOBAL_YAML=/model/<path_to_global_config_yaml> \
-e COTATRON_VC_YAML=/model/<path_to_vc_config_yaml> \
-e COTATRON_REF_YAML=/model/<path_to_ref_speaker_yaml> \
-e COTATRON_MODEL=/model/<path_to_checkpoint> \
cotatron:<version>-server
```

## Example

Server
```bash
docker run --gpus device=0 -d -v /raid/swpark/cotatron:/model -p 30002:30002 -e COTATRON_GLOBAL_YAML=/model/config/global/2cha_ssh.yaml -e COTATRON_VC_YAML=/model/config/vc/2cha_vc.yaml -e COTATRON_REF_YAML=/model/config/server/2cha_test.yaml -e COTATRON_MODEL=/model/chkpt/vc/2cha_revencoder_ssh/3bfa6b2epoch\=540.ckpt cotatron:0.1.5-server
```

Client
```bash
python synthesizer_client.py -r 10.122.64.97:30002 -i temp/valaudio/kss/2_1148.wav -t "제가 세살 때 엄마가 돌아가셔서, 엄마에 대한 기억은 희미해요." -s 0
```

## Training

WIP

## Author

브레인 박승원 수석
